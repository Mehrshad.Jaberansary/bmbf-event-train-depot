from minio import Minio
import pandas as pd
import os
#from minio.error import S3Error


def main():
    # Create a client with the MinIO server playground, its access key
    # and secret key.

    minio_address = str(os.environ['MINIO_ADDRESS'])
    minio_port = str(os.environ['MINIO_PORT'])
    minio_access_key = str(os.environ['MINIO_ACCESS'])
    minio_secret_key = str(os.environ['MINIO_SECRET'])
    minio_bucket_name = str(os.environ['MINIO_BUCKET_NAME'])
    minio_object_name = str(os.environ['MINIO_OBJECT_NAME'])
    minio_secure=bool(str(os.environ['SECURE']))

    #minio_address = "localhost"
    #minio_port = "9000"
    #minio_access_key = "minioadmin"
    #minio_secret_key = "minioadmin"
    #minio_bucket_name = "malaria"
    #minio_object_name = "reported_numbers.csv"


    client = Minio(
        '{0}:{1}'.format(minio_address, minio_port),
        access_key=minio_access_key,
        secret_key=minio_secret_key,
        secure=minio_secure
    )
    #list buckets
    #buckets = client.list_buckets()
    #print(buckets)

    if client.bucket_exists("malaria"):
        print("my-bucket exists")
    else:
        print("my-bucket does not exist")

    # List objects information.
    objects = client.list_objects("malaria", recursive=True, prefix="reported")
    for obj in objects:
        print(obj)

    # Get data of an object.
    try:
        response = client.get_object(minio_bucket_name, minio_object_name)
        df = pd.read_csv(response)

        # Read data from response.
    finally:
        response.close()
        response.release_conn()

    print(df.head())

if __name__ == "__main__":
    main()
