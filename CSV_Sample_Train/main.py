import pandas as pd
import os

## Get the env vars
csv_url = str(os.environ['CSV_URL'])

## Define directory of output
here = os.path.dirname(os.path.abspath(__file__))
out_dir = os.path.join(here, 'output')
result_path = os.path.join(out_dir, 'result.csv')
if not os.path.exists(out_dir):
    os.makedirs(out_dir)

## initialize
df_result = pd.DataFrame()

## read the existing result file from the previous run
try:
    df_result = pd.read_csv(result_path)
except:
    pass

## read the current station csv file
df_current_station = pd.read_csv(csv_url)

## analyze - start
df_result = pd.concat([df_result, df_current_station], ignore_index=True)
## analyze - end


## store df_result
df_result.to_csv(result_path, index=False)
