import os
import os.path as osp
import glob
from fhirpy import SyncFHIRClient

## output dir
here = osp.dirname(osp.abspath(__file__))
out_dir = osp.join(here, 'output')
if not osp.exists(out_dir):
    os.makedirs(out_dir)

## output file
if not osp.exists(osp.join(out_dir, 'report.txt')):
    with open(osp.join(out_dir, 'report.txt'), 'w') as f:
        pass

## helper function
def build_url(host, protocol='http', port=None, path=None):
    url = '{}://{}'.format(protocol, host)
    if port is not None:
        port = str(port)
        if len(port) > 0:
            url += ":" + port
    if path is not None:
        url += "/"
        url += path
    return url

## Define (input) variables from Docker Container environment variables
fhir_server = str(os.environ['FHIR_SERVER'])
fhir_port = str(os.environ.get('FHIR_PORT', ""))
fhir_protocol = str(os.environ.get('FHIR_PROTOCOL', "http"))

## FHIR API
fhir_api = build_url(host=fhir_server, protocol=fhir_protocol, port=fhir_port, path='fhir')

## Create an FHIRClient instance
## https://github.com/beda-software/fhir-py
client = SyncFHIRClient(fhir_api)


resources = ['Patient', 'Media', 'Observation']

## Collect Data Statistic
## Search for Resource
with open(osp.join(out_dir, 'report.txt'), 'a') as f:
    for resource in resources:
        count = 0
        items = client.resources(resource)
        for item in items:
            count = count + 1
        print("Number of '{}': {}".format(resource, count))    
        f.write("Number of '{}': {} \n".format(resource, count))


print("Done")
