# BMBF Event - PHT PADME Train Depot

All trains are stored in our Train Depot. This Train Depot is a git repository containing all PADME trains. If you don't have access yet, please feel free to contact us. This requires a DFN-AAI-based Login from an Institution participating in NFDI4Ing or a GitHub account.

To make a train available in PADME, you simply need to create a new folder in the train depot, containing your code and the Dockerfile. On every commit to the repository, changed trains will be automatically build and made available in PADME.

The build progress of your train can be monitored via the CI/CD menu in GitLab.

If you need to change your train at a later point in time, simply change the code and docker image, and everything will be rebuild and updated automatically.

## Specifying Variables

To allow the configuration of your train docker images, we use environment variables. Whenever a Train arrives at a station, values for these variables need to be specified by the station software user. This feature can for example be used to configure connection strings, names of datasets, credentials, etc.

This envs.json file  contains a description of the supported environment variables in form of a JSON array. For each environment variable, the following needs to be provided:

```
{
  "name": "the name of the variable",
  "type": "number|password|text|url|select",
  "required": true,
  "options": [
    "only needed",
    "when using select"
  ]
}
```

The property 'name' is the name that gets displayed to the station software user. Please be aware that since the name will be used as an environment variable, it cannot contain any spaces. Besides the name, we also support five different types: number, password, text, url, and select. Depending on the type, a different visualization is used in the station software. For example, a password will not be visible in plain text. When using the 'select' type, you can provide the selectable options via the 'options' array. Moreover, it is possible to mark variables as required.

You can see an example [here](https://git.rwth-aachen.de/Mehrshad.Jaberansary/bmbf-event-train-depot/-/blob/main/FHIR_Sample_Train/envs.json). The envs.json file is located next to Dockerfile in your Train main directory and is automatically attached to the Train as a label during the build process.

Now you are all set for creating trains in PADME. If you have further questions please do not hesitate to contact us.

## Datasets
### ISIC 2019
The whole dataset containing 33569 patient-related data was randomly and equally split across three different stations. The dataset is available in CSV format at each station and can be provided to a Train via a link. The following table defines the structure of the CSV files. Datasets are not clean; except for the 'id' column, the other columns may have null values.
| id | age_approx | anatom_site_general | lesion_id | sex | image_url | label |
|----|------------|---------------------|-----------|-----|-----------|-------|
|    |            |                     |           |     |           |       |


```python
## anatom_site_general
array(['anterior torso', 'upper extremity', 'posterior torso',
       'lower extremity', 'lateral torso', 'head/neck',
       'palms/soles', 'oral/genital', nan], dtype=object)

## sex
array(['female', 'male', nan], dtype=object)

## label
array(['female', 'male', nan], dtype=object)
```

### Malaria
| Country | Year | No. of cases_estimated | No. of deaths_estimated | No. of cases_median_estimated | No. of cases_min_estimated | No. of cases_max_estimated | No. of deaths_median_estimated | No. of deaths_min_estimated | No. of deaths_max_estimated | No. of cases_at_risk | No. of cases_reported | No. of deaths_reported | WHO Region |
|---------|------|------------------------|-------------------------|-------------------------------|----------------------------|----------------------------|--------------------------------|-----------------------------|-----------------------------|----------------------|-----------------------|------------------------|------------|
|         |      |                        |                         |                               |                            |                            |                                |                             |                             |                      |                       |                        |            |



## Tasks

### Task 1.
Consider the ISIC 2019 dataset. Based on the available columns, define a cohort (i.e., number of male patients with specific diagnostic category) and create a train that calculates the size of that cohort at each station.

Feel free to use the sample provided ([Task1_Sample_Train](https://git.rwth-aachen.de/Mehrshad.Jaberansary/bmbf-event-train-depot/-/tree/main/Task1_Sample_Train)) to get started. Please don't forget to copy and rename it first.

Note that the Train code must only store the aggregated results at each station; otherwise, the station managers will not approve the Train.

## Links
https://websites.fraunhofer.de/PersonalHealthTrain
