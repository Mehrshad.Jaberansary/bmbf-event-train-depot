import pandas as pd
import os

### TASK 1 ###

## Step1: Load the needed environment variables (i.e., CSV_URL)
csv_url = str(os.environ['CSV_URL'])

## Step2: Define a output directory to store your results


## Step3: Read the existing result file from the previous run if it is needed


## Step4: Load the current station csv file from the URL
df_current_station = pd.read_csv(csv_url)

## Step5: Analysis - count cohort size


## Step6: Store the results to the output directory


print("Successfully executed")
